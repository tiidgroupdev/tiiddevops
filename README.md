# Tiiddevops

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.0.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Referencia
https://gitlab.com/platzi-gitlab-live/platzi-devops/-/tree/master
https://gitlab.com/platzi-live-group/devops-project/-/tree/master

## Validar YML
https://gitlab.com/tiidgroupdev/tiiddevops/-/ci/lint

## Configuracion de Git

--Ver todo
git config --list

--Ver datos
git config --get user.name
git config --get user.email

--Obtener Git Remote
git branch rama origin/rama

--Seleccionar Git
git checkout rama

--Subir los cambios en Git
git status
git add --all
git commit -m "Configuracion tempalte issues - bug"
git push
